package studio.vegabyte.minecraft.spigot.deathfinitive_control.items;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.CompassMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import studio.vegabyte.minecraft.spigot.deathfinitive_control.App;
import studio.vegabyte.minecraft.spigot.deathfinitive_control.services.ConfigurationService;

/**
 * Utilities for the the Death Compass item
 */
public class DeathCompassItem {
  // #region variables

  private NamespacedKey keyIs;

  // prefab of the compass
  private ItemStack prefab;

  // #endregion

  // #region private - methods

  /**
   * Update the item meta of a death compass using a death event.
   */
  private void updateItemMetaLore(ItemMeta meta, PlayerDeathEvent playerDeathEvent) {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy h:mm:ss a");
    LocalDateTime now = LocalDateTime.now();

    meta.setLore(new ArrayList<String>() {
      {
        add(String.format("Player: %s", playerDeathEvent.getEntity().getDisplayName()));
        add(String.format("Message: %s", playerDeathEvent.getDeathMessage()));
        add(String.format("Location: %s (%s / %s / %s)",
            playerDeathEvent.getEntity().getLocation().getWorld().getName(),
            playerDeathEvent.getEntity().getLocation().getBlockX(),
            playerDeathEvent.getEntity().getLocation().getBlockY(),
            playerDeathEvent.getEntity().getLocation().getBlockZ()));
        add(String.format("Date/ Time: %s", dtf.format(now)));
      }
    });
  }

  /**
   * Updat the compass's lodestone using a death event.
   */
  public void updateLodestone(ItemStack compassItemStack, PlayerDeathEvent playerDeathEvent) {
    CompassMeta compassMeta = (CompassMeta) compassItemStack.getItemMeta();
    compassMeta.setLodestoneTracked(false); // ~ yes, false
    compassMeta.setLodestone(playerDeathEvent.getEntity().getLocation());
    compassItemStack.setItemMeta(compassMeta);
  }

  // instantiate a new compass
  public ItemStack Instantiate(PlayerDeathEvent playerDeathEvent) {
    ItemStack instance = new ItemStack(this.prefab); // instantiate prefab

    // set meta - persistent data
    ItemMeta meta = instance.getItemMeta();
    PersistentDataContainer data = meta.getPersistentDataContainer();
    data.set(this.keyIs, PersistentDataType.BOOLEAN, true);
    updateItemMetaLore(meta, playerDeathEvent);
    instance.setItemMeta(meta);

    this.updateLodestone(instance, playerDeathEvent);

    return instance;
  }

  // check if the item is a DeathCompassItem
  public boolean IsCompass(ItemStack item) {
    return item != null && item.getItemMeta().getPersistentDataContainer().has(keyIs, PersistentDataType.BOOLEAN);
  }
  // #endregion

  // #region private - compass

  /**
   * Create the prefab {@link ItemStack} from which instances will be created.
   */
  private void createCompassPrefab() {
    ItemStack prefab = new ItemStack(Material.COMPASS, 1);

    // set meta
    ItemMeta nextItemMeta = prefab.getItemMeta();

    // set meta - display
    nextItemMeta.setDisplayName("Death Compass");
    nextItemMeta.setLore(new ArrayList<String>() {
      {
        add("Use to find where a player died.");
      }
    });

    prefab.setItemMeta(nextItemMeta);

    this.prefab = prefab;
  }

  // #endregion

  // #region Lifecycle

  private App app;
  private ConfigurationService configurationService;

  public DeathCompassItem(App app, ConfigurationService configurationService) {
    this.app = app;
    this.configurationService = configurationService;

    keyIs = new NamespacedKey(app, "is");

    createCompassPrefab();
  }

  // #endregion
}
