package studio.vegabyte.minecraft.spigot.deathfinitive_control.managers;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import studio.vegabyte.minecraft.spigot.deathfinitive_control.App;
import studio.vegabyte.minecraft.spigot.deathfinitive_control.items.DeathCompassItem;

public class DeathCompassManager implements Listener {

  // #region handlers

  @EventHandler
  private void onPlayerInteract(PlayerInteractEvent event) {
    Action action = event.getAction();
    ItemStack item = event.getItem();
    Block block = event.getClickedBlock();

    // filter for "right click lodestone with a death compass" event
    if (action != Action.RIGHT_CLICK_BLOCK ||
        item == null ||
        !this.compassItem.IsCompass(item) ||
        block.getType() != Material.LODESTONE)
      return;

    // cancel event, so the compass is note updated
    event.setCancelled(true);
  }

  // #endregion

  // #region lifecycle

  private App app;
  private DeathCompassItem compassItem;

  public DeathCompassManager(App app, DeathCompassItem compassItem) {
    this.app = app;
    this.compassItem = compassItem;

    this.app.getServer().getPluginManager().registerEvents(this, this.app); // register events
  }

  // #endregion

}
