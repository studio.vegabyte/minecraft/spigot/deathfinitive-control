package studio.vegabyte.minecraft.spigot.deathfinitive_control;

public enum Outcome {
  KEEP, DROP, VANISH
}